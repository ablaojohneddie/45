import React from 'react';


const UserContext = React.createContext();
//create a context object
//a context as the name states

export const UserProvider = UserContext.Provider;

export default UserContext;
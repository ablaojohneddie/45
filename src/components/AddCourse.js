import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddCourse({fetchData}) {

	//Add state for the forms of course
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	//States for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	//Functions to handle opening and closing of our AddCourse Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false)



	//Function for fetching the create course in the backend
	const addCourse = (e) => {
		e.preventDefault();

		fetch('http://localhost:4000/courses/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				//Show a success message
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course successfully added.'
				})

				//Close our modal
				closeAdd();
				//Render the updated data using the detchData prop
				fetchData();
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: 'Something went wrong. Please Try again'
				})
				closeAdd();
				fetchData();
			}

			//Clear out the input fields
			setName("")
			setDescription("")
			setPrice("")


		})
	}


	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New Course</Button>

		{/*Add Modal Forms*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addCourse(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>

		)
}
